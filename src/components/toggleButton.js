import React, {Component} from 'react';
import {View, Text, TouchableOpacity} from 'react-native';

export default class ToggleButton extends Component {
  constructor(props) {
    super(props);
    this.state = {
      toggle: false,
    };
  }

  handlePress = () => {
    const newState = !this.state.toggle;
    this.setState({toggle: newState});
  };

  render() {
    const {toggle} = this.state;
    const textValue = toggle ? 'ON' : 'Off';
    const buttonBg = toggle ? 'dodgerblue' : 'white';
    const textColor = toggle ? 'white' : 'black';
    return (
      <View style={{flexDirection: 'row'}}>
        <TouchableOpacity
          onPress={() => this.handlePress()}
          style={{
            flex: 1,
            height: 60,
            margin: 10,
            backgroundColor: buttonBg,
            justifyContent: 'center',
            borderColor: 'dodgerblue',
            borderWidth: 2,
          }}>
          <Text style={{color: textColor, textAlign: 'center', fontSize: 16}}>
            {textValue}
          </Text>
        </TouchableOpacity>
      </View>
    );
  }
}
