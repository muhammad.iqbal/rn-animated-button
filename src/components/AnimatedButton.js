import React, {useState} from 'react';
import {View, TouchableOpacity, Text} from 'react-native';
import styled from 'styled-components/native';
const animatedButton = (props) => {
  //states
  const [toggle, setToggle] = useState(false);

  const {title} = props;

  return (
    <View style={{flexDirection: 'row'}}>
      <TouchableOpacity
        onPress={() => {
          setToggle(!toggle);
        }}
        style={{
          flex: 1,
          height: 60,
          margin: 10,
          backgroundColor: 'dodgerblue',
          justifyContent: 'center',
        }}>
        <Text style={{color: 'white', textAlign: 'center', fontSize: 16}}>
          Hello
        </Text>
      </TouchableOpacity>
    </View>
  );
};

const Outer = styled.View`
  flex: row;
`;

const Button = styled.TouchableOpacity`
  border: 1px solid black;
  border-radius: 3px;
  width: 100px;
  height: 30px;
  display: flex;
  transition: all 0.5s ease;
  background: black;
`;

export const Box = styled.View`
  display: inline-block;
  background: pink;
  width: 200px;
  height: 200px;
  transition: transform 300ms ease-in-out;

  &:hover {
    transform: translate(200px, 150px) rotate(20deg);
  }
`;

const RotatedBox = styled.View`
  transform: rotate(90deg);
  text-shadow-offset: 10px 5px;
  font-variant: small-caps;
  margin: 5px 7px 2px;
`;

export default animatedButton;

{
  /* <div class="button">
    <div class="text">button</div>
  </div> */
}
